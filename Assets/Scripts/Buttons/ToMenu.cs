﻿using UnityEngine;

public class ToMenu : MonoBehaviour
{


    public void LoadMenu()
    {

        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "MainScene")
            UnityEngine.SceneManagement.SceneManager.LoadScene("MenuScene");

    }

}
