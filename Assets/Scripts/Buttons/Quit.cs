﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quit : MonoBehaviour
{

    private void OnMouseDown()
    {
        GameManager.instance.ingameUIMan.ExitToMenu();
    }

}
