﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Component required by the PlayerController() to control the camera.
/// </summary>
public class CameraController : MonoBehaviour
{

    #region Variables

    #region Public

    public static CameraController instance;

    public Transform whiteBoard, pauseCameraPos;

    public float zoomInSpeed = 10;

    public float rotateSpeed = 7;

    public float rotateSlowDistance = 2;

    [HideInInspector] public Quaternion originalRotation;

    [HideInInspector] public float travelDistance = 0f;

    [HideInInspector] public bool pausingGame = false;

    [HideInInspector] public bool resumingGame = false;

    #endregion

    #region Private

    private Vector2 mouseLook, smoothV, gyroInput;

    private Transform playerTransform;

    #endregion

    #endregion

    private void Awake()
    {

        playerTransform = transform.parent.transform.parent.transform;

        instance = this;

    }

    private void Start()
    {
        if (PlayerController.instance == null)
        {
            Debug.Log("There needs to be a Player Controller on the parent object for this script to work");
        }

        // Set original player position
        mouseLook.x = playerTransform.localEulerAngles.y; // set original player position
        CameraController.instance.originalRotation = CameraController.instance.transform.rotation;
        CameraController.instance.travelDistance = (Vector3.Distance(CameraController.instance.pauseCameraPos.transform.position, CameraController.instance.transform.parent.position));

    }


    public void SetInput(float x, float y)
    {

        gyroInput.x = x;
        gyroInput.y = y;

    }


    private void Update()
    {

        if (GameManager.instance.isLoading ) //|| GameManager.instance.isDrawing)
            return;

        // Skip the update if game is paused
        if (GameManager.instance.isPaused)
        {
            if (pausingGame)
            {
                MoveToPausePosition();
            }
        }
        else
        {
            if (resumingGame)
            {
                MoveToPlayerPosition();
                RotateToOriginalPosition();
            }
            else if (transform.localPosition != Vector3.zero)
            {
                RotateToOriginalPosition();
                MoveToPlayerPosition();
            }
            else if (Input.mousePosition != Vector3.zero & !GameManager.instance.isDrawing)
            { RotateCamera(); }

        }

    }


    public void MoveToPausePosition()
    {

        playerTransform.GetComponent<Rigidbody>().isKinematic = true;

        // Move our position a step closer to the target.
        transform.position = Vector3.MoveTowards(transform.position, pauseCameraPos.position, zoomInSpeed * Time.deltaTime);

        Quaternion neededRotation = Quaternion.LookRotation(whiteBoard.position - transform.position);

        transform.rotation = Quaternion.Slerp(transform.rotation, neededRotation, Time.deltaTime * rotateSpeed);

        //transform.rotation = Quaternion.RotateTowards(transform.rotation, neededRotation, Time.deltaTime * rotateSpeed);


        Vector3 offset = pauseCameraPos.position - transform.position;
        float sqrLen = offset.sqrMagnitude;

        // square the distance we compare with
        if (sqrLen < 0.5 * 0.5)
        {
            if (Quaternion.Angle(neededRotation, transform.rotation) <= 0.1)
            {
                pausingGame = false;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }
    }

    public void MoveToPlayerPosition()
    {


        if (Vector3.Distance(transform.position, transform.parent.position) <= (travelDistance / rotateSlowDistance))
        {
            RotateToOriginalPosition();
        }
        else
        {

            transform.position = Vector3.MoveTowards(transform.position, transform.parent.position, zoomInSpeed * Time.deltaTime);

            Quaternion neededRotation = Quaternion.LookRotation(transform.parent.position - transform.position);

            transform.rotation = Quaternion.Slerp(transform.rotation, neededRotation, Time.deltaTime * rotateSpeed);

            //transform.rotation = Quaternion.RotateTowards(transform.rotation, neededRotation, Time.deltaTime * rotateSpeed);

        }

    }


    public void RotateToOriginalPosition()
    {

        transform.position = Vector3.MoveTowards(transform.position, transform.parent.position, zoomInSpeed * Time.deltaTime);

        transform.rotation = Quaternion.Slerp(transform.rotation, originalRotation, (Time.deltaTime * rotateSpeed) / 2); // Slow rotation

        //transform.rotation = Quaternion.RotateTowards(transform.rotation, originalRotation, Time.deltaTime * rotateSpeed * 10);


        if (Quaternion.Angle(originalRotation, transform.rotation) <= 0.1f)
        {
            transform.rotation = originalRotation;
            resumingGame = false;
            playerTransform.GetComponent<Rigidbody>().isKinematic = false;
        }

    }


    public void SetMouseLookX(float x)
    {
        mouseLook.x = x;
    }


    private void RotateCamera()
    {
        Vector2 mouseInput;

        if (PlayerController.instance.mouseEnabled)
        {

            mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        }
        else
        {
            mouseInput = gyroInput;

        }

        mouseInput = Vector2.Scale(mouseInput, new Vector2(PlayerController.instance.sensitivity * PlayerController.instance.smoothing, PlayerController.instance.sensitivity * PlayerController.instance.smoothing));

        smoothV.x = Mathf.Lerp(smoothV.x, mouseInput.x, 1f / PlayerController.instance.smoothing);

        smoothV.y = Mathf.Lerp(smoothV.y, mouseInput.y, 1f / PlayerController.instance.smoothing);

        mouseLook += smoothV;

        mouseLook.y = Mathf.Clamp(mouseLook.y, PlayerController.instance.MaxDownAngle, PlayerController.instance.MaxUpAngle);

        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        playerTransform.localRotation = Quaternion.AngleAxis(mouseLook.x, playerTransform.up);

    }
}