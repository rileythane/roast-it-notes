﻿using UnityEngine;

/// <summary>
/// Component used to display the name of the object the PlayerController() is looking at.
/// </summary>
public class ObjectStatus : MonoBehaviour
{

    public Font guiFont;

    string lastObject;

    int adjustedFontSize;

    public LayerMask namedObjectsLayers; // These are the layers the status will pop up for

    void Update()
    {

        if (GameManager.instance.isPaused || GameManager.instance.isLoading)
            return;

        ObjectCheck();

    }

    public void ObjectCheck()
    {

        // Check if game is paused and exit function if true.
        if (GameManager.instance.isPaused)
            return;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (GameManager.instance.isDrawing)
        {
            lastObject = "Leave 'em a message!!";
        }
        else { 

            if (Physics.Raycast(ray, out hit, 5, namedObjectsLayers))
            {
                Debug.DrawLine(ray.origin, hit.point, Color.red);

                if (hit.transform.gameObject.name == lastObject)
                {
                    return;

                }
                else if (hit.collider.transform.gameObject.name != lastObject)
                {
                    lastObject = hit.collider.transform.gameObject.name;
                }
            }
            else
            {
                lastObject = "";
            }
        }

    }


    private void OnGUI()
    {
        if (lastObject == "" || lastObject == null || GameManager.instance.isPaused)
            return;

        adjustedFontSize = Screen.width / 30;

        GUI.skin.font = guiFont;
        GUI.skin.box.fontSize = adjustedFontSize;
        GUI.skin.box.alignment = TextAnchor.MiddleCenter;

        GUI.Box(new Rect(Screen.width / 2 - ((adjustedFontSize * lastObject.Length) / 2), adjustedFontSize + 10, adjustedFontSize * lastObject.Length, adjustedFontSize * 1.5f), lastObject);
    }

}