﻿using UnityEngine;
using System.Runtime.InteropServices;

/// <summary>
/// First Person Controller
/// </summary>
public class PlayerController : MonoBehaviour
{

    #region Variables

    #region Dll Import
    [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
    public static extern void mouse_event(long dwFlags, long dx, long dy, long cButtons, long dwExtraInfo);
    private const int MOUSEEVENTF_LEFTDOWN = 0x02;
    private const int MOUSEEVENTF_LEFTUP = 0x04;
    private const int MOUSEEVENTF_MOVE = 0x0001;
    #endregion

    #region Public

    // Speed
    [Header("Player Settings")]
    public float movementSpeed = 3.0f;

    // Throwing
    public float throwForce = 10;

    // Sensitivity
    [Header("Mouse Settings")]
    public float sensitivity = 1.5f;
    public float smoothing = 1.0f;

    // If mouse should be used (Gyro/Controller support)
    public bool mouseEnabled = true;

    // Used to clamp looking up and down
    [Header("Look Up/Down Clamping")]

    public float MaxUpAngle = 60;
    public float MaxDownAngle = -75;


    public static PlayerController instance;

    [HideInInspector]
    public DragableObject currentObject;

    #endregion

    #region Private

    float virtualHorizontal;
    float virtualVertical;

    #endregion

    #endregion


    private void Awake()
    {

        instance = this;

    }

    private void Start()
    {
        if (CameraController.instance == null)
        {
            Debug.Log("There needs to be a Camera Controller on the child object for this script to work");
        }
    }


    public void UpdateVirtualAxis(float x, float y)
    {

        virtualHorizontal = x;
        virtualVertical = y;

    }


    private void Update()
    {

        // Skip the update if game is paused
        if (GameManager.instance.isPaused || CameraController.instance.resumingGame || GameManager.instance.isLoading || GameManager.instance.isDrawing)
            return;

        MovePlayer();

    }


    private void MovePlayer()
    {
        float translation, straffe;

        if (mouseEnabled)
        {
            translation = Input.GetAxis("Vertical") * movementSpeed;

            straffe = Input.GetAxis("Horizontal") * movementSpeed;
        }
        else
        {

            translation = virtualVertical * movementSpeed;

            straffe = virtualHorizontal * movementSpeed;

        }

        translation *= Time.deltaTime;
        straffe *= Time.deltaTime;

        transform.Translate(straffe, 0, translation);
    }


    public void SimMouseDown()
    {
        mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
    }


    public void SimMouseUp()
    {
        mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
    }
}
