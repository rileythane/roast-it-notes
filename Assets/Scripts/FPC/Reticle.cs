﻿using UnityEngine;

/// <summary>
/// Display a Reticle(dot) in the centre of the screen.
/// </summary>
public class Reticle : MonoBehaviour
{

    public Texture2D crosshairTex;
    public Color crosshairColor;

    public int crosshairSize = 15;

    Vector2 startPos;

    private void Start()
    {
        if (Application.isEditor)
        {
            startPos = new Vector2(Screen.width / 2 - crosshairSize / 2, Screen.height / 2 - 20 - (crosshairSize / 2));
        }
        else
        {
            startPos = new Vector2(Screen.width / 2 - (crosshairSize / 2), Screen.height / 2 - (crosshairSize / 2));
        }
    }



    void OnGUI()
    {

        if (GameManager.instance.isPaused || GameManager.instance.isLoading || GameManager.instance.isDrawing)
            return;

        GUI.contentColor = crosshairColor;
        GUI.DrawTexture(new Rect(startPos.x, startPos.y, 15, 15), crosshairTex, 0, true, 10, crosshairColor, 0, 0);

    }
}
