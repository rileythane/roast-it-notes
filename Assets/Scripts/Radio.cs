﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radio : MonoBehaviour
{

    public AudioSource backgroundRadioSource, interactionRadioSource;

    // Use this for initialization
    void Start()
    {

        AudioManager.instance.backgroundRadioSource = backgroundRadioSource;

        AudioManager.instance.interactionRadioSource = interactionRadioSource;

    }

}
