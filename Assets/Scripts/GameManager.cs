﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager instance = null;

    public Transform roastItSpawner;

    public GameObject roastItPrefab;

    [HideInInspector]
    public InGameUI ingameUIMan = null;

    //[HideInInspector]
    public bool isLoading = false;

    [HideInInspector]
    public bool isDrawing = false;

    [HideInInspector]
    public bool canPaint = false;

    [HideInInspector]
    public bool isPaused = true;
    
    private void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

    }

    public void TogglePause()
    {
        isPaused = !isPaused;

        ingameUIMan.TogglePauseMenu();

        if (isPaused) // Do anything after pausing the game here
        {

            //Cursor.lockState = CursorLockMode.None;
            //Cursor.visible = true;

            // Set the look position of the camera before pause.
            CameraController.instance.originalRotation = CameraController.instance.transform.rotation;
            CameraController.instance.travelDistance = (Vector3.Distance(CameraController.instance.pauseCameraPos.transform.position, CameraController.instance.transform.parent.position));

            CameraController.instance.pausingGame = true;

        }
        else // Do anything after resuming the game here
        {

            if (!isDrawing)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

            CameraController.instance.pausingGame = false;

            if (!isLoading)
                CameraController.instance.resumingGame = true;

        }

    }
}
