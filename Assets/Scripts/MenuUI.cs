﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuUI : MonoBehaviour
{
    public GameObject settingsPanel;
    public GameObject menuPanel;
    
    public AudioSource menuAudio;
    public AudioClip ethanEgg;

    public Slider sensitivitySlide, movementSlide;

    public Animator fadeAnimator;

    public GameObject loading;

    public Slider loadingProgress;

    public GameObject[] buttons;


    private void Start()
    {
        StartCoroutine(ShowControls());
    }

    IEnumerator ShowControls()
    {
        yield return new WaitForSeconds(1);

        foreach (GameObject button in buttons)
        {

            button.SetActive(true);

        }

    }


    public void InitSettings()
    {

        SetSensitiviy();
        SetMovementSpeed();

    }

    public void ToggleSettings()
    {
        //if (!GameManager.instance.isLoading)
            //settingsPanel.SetActive(!settingsPanel.activeSelf);
            //menuPanel.SetActive(!menuPanel.activeSelf);

    }

    public void ToggleInvertXAxis()
    {
        //PlayerController.instance.inverseGyroXAxis = !GyroController.instance.inverseGyroXAxis;
    }


    public void ToggleInvertYAxis()
    {
        //PlayerController.instance.inverseGyroYAxis = !PlayerController.instance.inverseGyroYAxis;
    }

    public void SetSensitiviy()
    {
        PlayerController.instance.sensitivity = sensitivitySlide.value;
    }

    public void SetMovementSpeed()
    {
        PlayerController.instance.movementSpeed = movementSlide.value;
    }

    public void QuitGame()
    {
        if (!GameManager.instance.isLoading)
        Application.Quit();
    }

    public void StartGame()

    {

        GameManager.instance.isLoading = true;

        foreach (GameObject button in buttons)
        {

            button.SetActive(false);

        }

        loading.SetActive(true);

        fadeAnimator.SetTrigger("Fade");
        
        StartCoroutine(LoadYourAsyncScene());

    }


    IEnumerator LoadYourAsyncScene()
    {

        yield return null;

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("MainScene");

        asyncLoad.allowSceneActivation = false;

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            loadingProgress.value = asyncLoad.progress;

            if (asyncLoad.progress >= 0.9f)
            {
                yield return new WaitForSeconds(3);
                asyncLoad.allowSceneActivation = true;
            }
            
            yield return null;
        }
    }

    public void PlaySound()
    {

        menuAudio.PlayOneShot(ethanEgg);
        //Application.OpenURL("https://rileythane.itch.io/roast-it-notes");
    }

}


