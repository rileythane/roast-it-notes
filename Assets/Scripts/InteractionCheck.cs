﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InteractionCheck : MonoBehaviour
{

    #region Variables

    #region Public

    [Header("Check Settings")]
    public CheckType interactionType;
    [TagSelector] public string targetObjectTag;
    public int requiredInteractions = 1;
    public GameObject postItPlaceholder;

    [Header("On Completion")]
    public GameObject[] postItRequests;
    public SpriteRenderer taskStrikethrough;

    [Header("Radio Interaction")]
    public AudioClip startInteraction;
    public AudioClip finishInteraction;

    #endregion

    #region Private

    private int interactions = 0;

    private bool hasAudioPlayed = false;

    #endregion

    #endregion

    public enum CheckType { IsOnEnter, IsOnExit };


    private void Start()
    {
        interactions = 0;
    }

    public void DoInteraction()
    {



        if (startInteraction != null & !hasAudioPlayed)
        {

            hasAudioPlayed = true;
            AudioManager.instance.interactionQueue.AddToQueue(startInteraction);

            AudioManager.instance.UpdateRadioPause();

            if (!AudioManager.instance.interactionQueue.IsPlaying())
            {
                AudioManager.instance.interactionQueue.PlayQueue();
            }

        }

        interactions++;

        if (interactions >= requiredInteractions)
        {

            postItPlaceholder.SetActive(true);

        }

    }


    public void CompleteInteraction()
    {


        taskStrikethrough.gameObject.SetActive(true);

        // Poof away post it here
        foreach (GameObject postIt in postItRequests)
        {
            postIt.SetActive(false);
        }

        postItPlaceholder.SetActive(false);

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(targetObjectTag) & interactionType == CheckType.IsOnEnter & interactions < requiredInteractions & !GameManager.instance.isLoading)
        {
            DoInteraction();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag(targetObjectTag) & interactionType == CheckType.IsOnExit & interactions < requiredInteractions & !GameManager.instance.isLoading)
        {
            DoInteraction();

        }
    }



}



