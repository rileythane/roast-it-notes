﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Move object and place on specified surfaces.
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class PlaceableObject : MonoBehaviour
{
    #region Variables

    #region Public

    [Header("Object Settings")]
    public LayerMask placeableOn;
    public bool placed = true;

    [Header("Object Sounds")]
    public AudioClip pickupSound;
    public AudioClip collideSound, dropSound;

    // float dragDepth; MOve to player controller or gamemanager

    [HideInInspector] public Transform targetPos;

    [HideInInspector] public bool dragging = false;

    [HideInInspector] public bool touchingObject;

    [HideInInspector] public int originalLayer;

    #endregion

    #region Private

    //private Transform parentTransform = null;

    private AudioSource objectAudioSource;

    public Rigidbody rb;

    public Transform parentTransform = null;

    private Collider[] colliders;

    Ray ray;

    RaycastHit hit;

    MeshRenderer placeHolderRenderer;

    #endregion

    #endregion

    public void Awake()
    {
        objectAudioSource = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
        originalLayer = gameObject.layer;

        colliders = GetComponents<Collider>();
    }


    public virtual void Place()
    {



        rb.isKinematic = true;

        // Check all colliders
        foreach (Collider col in colliders)
        {
            col.isTrigger = true;
        }

        if (parentTransform != null)
            transform.parent = parentTransform;

        placed = true;
        dragging = false;

        if (placeHolderRenderer != null)
        {

            placeHolderRenderer.transform.GetComponent<Placeholder>().interaction.CompleteInteraction();
            placeHolderRenderer = null;
        }

    }

    public void Pickup()
    {

        placed = false;
        dragging = true;
        gameObject.layer = LayerMask.NameToLayer("Handling");

        if (transform.parent != null)
            transform.parent = null;

        if (pickupSound != null)
            objectAudioSource.PlayOneShot(pickupSound);

    }

    public void Drop()
    {

        rb.isKinematic = false;

        foreach (Collider col in colliders)
        {
            col.isTrigger = false;
        }

        dragging = false;
        gameObject.layer = originalLayer;

    }


    public void OnCollisionEnter(Collision collision)
    {
        //Check how fast the obejct is moving then play sound if force is strong enough
        if (collision.relativeVelocity.magnitude > AudioManager.instance.requiredCollisionForce & collideSound != null)
        {
            objectAudioSource.PlayOneShot(collideSound);
        }

    }


    public virtual void OnMouseDown()
    {

        if (GameManager.instance.isLoading)
            return;

        Pickup();
        
    }


    public virtual void OnMouseUp()
    {

        rb.velocity = Vector3.zero; // kill previous momentum
        rb.angularVelocity = Vector3.zero;

        if (touchingObject)
        {
            Place();
        }
        else
        {
            Drop();
        }

        if (dropSound != null)
            objectAudioSource.PlayOneShot(dropSound);

    }


    public virtual void OnMouseDrag()
    {

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 2, placeableOn))
        {

            if (hit.collider.gameObject.CompareTag("Placeholder") & CompareTag("RoastIt"))
            {
                transform.position = hit.transform.position;

                transform.rotation = hit.transform.rotation;

                if (placeHolderRenderer == null)
                {
                    placeHolderRenderer = hit.transform.GetComponent<MeshRenderer>();
                    placeHolderRenderer.enabled = false;
                    placeHolderRenderer.transform.GetChild(0).gameObject.SetActive(false);
                    placeHolderRenderer.transform.GetChild(1).gameObject.SetActive(false);
                }

                parentTransform = hit.transform.parent.transform;


            }
            else
            {
                transform.position = hit.point;
                transform.forward = new Vector3(-hit.normal.x, -hit.normal.y, -hit.normal.z);

                if (placeHolderRenderer != null)
                {
                    placeHolderRenderer.enabled = true;

                    placeHolderRenderer.transform.GetChild(0).gameObject.SetActive(true);
                    placeHolderRenderer.transform.GetChild(1).gameObject.SetActive(true);

                    placeHolderRenderer = null;
                }

                parentTransform = hit.transform;

            }

            touchingObject = true;
            //parentTransform = hit.transform;

        }
        else
        {

            transform.position = CameraController.instance.transform.GetChild(0).position;
            transform.rotation = PlayerController.instance.transform.rotation;

            touchingObject = false;
            parentTransform = null;

        }
    }

}
