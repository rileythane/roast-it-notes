﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoastItObject : PlaceableObject
{


    #region Variables

    #region Private

    //private Rigidbody rb;

    private bool isBlank = true;

    #endregion

    #endregion

    //public override void Place()
    //{
    //    //rb.isKinematic = true;
    //    // TODO check all colliders?

    //    //GetComponent<Collider>().isTrigger = true;


    //    if (gameObject.CompareTag("RoastIt") & parentTransform.gameObject.layer == LayerMask.NameToLayer("Placeholder"))
    //    {

    //        parentTransform.gameObject.GetComponent<Placeholder>().interaction.CompleteInteraction();
    //        transform.parent = parentTransform.transform.parent.transform;
    //        placed = true;

    //    }
    //    else
    //    {

    //        base.Place();

    //    }

    //}


    private void StartDrawing()
    {

        GameManager.instance.isDrawing = true;

        // Freeze the player
        PlayerController.instance.GetComponent<Rigidbody>().isKinematic = true;

        // Enable Mouse
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;


        // De activate animation
        transform.GetChild(1).gameObject.SetActive(false);

        CameraController.instance.transform.localRotation = Quaternion.identity;
        transform.position = CameraController.instance.transform.GetChild(1).position;
        transform.rotation = PlayerController.instance.transform.rotation;

        transform.GetChild(0).gameObject.SetActive(true);         // Activate the spacebar object

        base.rb.isKinematic = true;
        GetComponent<MeshCollider>().convex = false;

        StartCoroutine(EnablePainting(0.5f));

    }


    IEnumerator EnablePainting(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        //enable painting
        GameManager.instance.canPaint = true;
        
    }


    void StopDrawing()
    {

        transform.GetChild(0).gameObject.SetActive(false); // hide spacebar

        GameManager.instance.canPaint = false; // disbale painting

        isBlank = false;

        // Move roast it to spawner position
        transform.position = GameManager.instance.roastItSpawner.GetChild(0).position;  //originalLocation + Vector3.up * 0.1f;
        transform.rotation = GameManager.instance.roastItSpawner.GetChild(0).rotation;  //originalRotation;

        //TODO make player look at roast it
        //Vector3 targetPos = GameManager.instance.roastItSpawner.GetChild(0).position;

        //targetPos.y = 0;

        //PlayerController.instance.transform.LookAt(targetPos);

        //CameraController.instance.SetMouseLookX(PlayerController.instance.transform.localEulerAngles.y);


        PlayerController.instance.GetComponent<Rigidbody>().isKinematic = false;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        GameManager.instance.isDrawing = false;

        //spawn a new roast it
        Instantiate(GameManager.instance.roastItPrefab, GameManager.instance.roastItSpawner.transform.position, GameManager.instance.roastItSpawner.transform.rotation);

    }


    private void Update()
    {


        if (GameManager.instance.isDrawing & isBlank)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {

                StopDrawing();

            }
        }



    }


    public override void OnMouseDown()
    {

        if (GameManager.instance.isLoading || GameManager.instance.isPaused)
            return;


        if (isBlank & !GameManager.instance.isDrawing)
        {
            StartDrawing();
        }
        else
        {
            // dragging = true;??
            base.Pickup();
        }
        
    }


    public override void OnMouseUp()
    {

        if (!GameManager.instance.isDrawing)
        {
            rb.isKinematic = false;
            GetComponent<MeshCollider>().convex = true;

            base.OnMouseUp();
        }

    }


    public override void OnMouseDrag()
    {

        if (!isBlank)
            base.OnMouseDrag();

    }
}
