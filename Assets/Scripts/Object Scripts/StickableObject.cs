﻿using UnityEngine;

/// <summary>
/// Drag a rigidbody with the mouse using a spring joint and stick to surfaces.
/// </summary>
public class StickableObject : DragableObject
{

    public bool stuck = false;


    public override void OnCollisionEnter(Collision collision)
    {
        if (!dragging & !stuck)
            Stick();

        base.OnCollisionEnter(collision);

    }



    public override void OnMouseDown()
    {

        if (GameManager.instance.isPaused || GameManager.instance.isLoading)
            return;

        if (stuck)
        {
            Unstick();
        }

        base.OnMouseDown();
    }

    public override void OnMouseDrag()
    {

        if (GameManager.instance.isLoading)
            return;

        HandleInput(Input.mousePosition);

    }

    public override void HandleInput(Vector3 screenPosition)
    {

        base.HandleInput(screenPosition);

    }

    public void Stick()
    {
        rb.isKinematic = true;
        stuck = true;
    }

    public void Unstick()
    {
        rb.isKinematic = false;
        stuck = false;
    }

}
