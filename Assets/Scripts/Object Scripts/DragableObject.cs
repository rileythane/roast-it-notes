﻿using UnityEngine;

/// <summary>
/// Drag a rigidbody with the mouse using a spring joint.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AudioSource))]
public class DragableObject : MonoBehaviour
{
    #region Variables

    #region Public
    [Header("Object Settings")]
    public LayerMask interactiveLayers;
    public float force = 600;
    public float damping = 6;

    public Transform centerOfMassLocation;

    [Header("Object Sounds")]
    public AudioClip pickupSound;
    public AudioClip collideSound, dropSound;

        
    [HideInInspector] public bool dragging = false;

    [HideInInspector] public Rigidbody rb;

    [HideInInspector] public ConfigurableJoint joint;

    #endregion

    #region Private

    private Transform jointTrans;

    private AudioSource objectAudioSource;

    private float dragDepth;

    private int originalLayer;

    #endregion

    #endregion

    public virtual void Start()
    {
        rb = GetComponent<Rigidbody>();

        objectAudioSource = GetComponent<AudioSource>();

        if (centerOfMassLocation)
            rb.centerOfMass = centerOfMassLocation.localPosition;

        originalLayer = gameObject.layer;

    }


    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) & dragging)
        {
            HandleThrow(PlayerController.instance.throwForce);
        }
    }


    public virtual void OnCollisionEnter(Collision collision)
    {
        //Check how fast the obejct is moving then play sound if force is strong enough
        if (collision.relativeVelocity.magnitude > AudioManager.instance.requiredCollisionForce & collideSound != null)
        {
            objectAudioSource.PlayOneShot(collideSound);
        }

    }


    public virtual void OnMouseDown()
    {

        if (GameManager.instance.isPaused || GameManager.instance.isLoading)
            return;

        HandleInputBegin(Input.mousePosition);

        if (pickupSound != null)
            objectAudioSource.PlayOneShot(pickupSound);

    }

    public virtual void OnMouseUp()
    {

        if (GameManager.instance.isLoading)
            return;

        HandleInputEnd();

    }

    public virtual void OnMouseDrag()
    {
        if (GameManager.instance.isLoading)
            return;

        HandleInput(Input.mousePosition);
    }

    public void HandleInputBegin(Vector3 screenPosition)
    {

        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 4, interactiveLayers))
        {

                dragDepth = CameraPlane.CameraToPointDepth(Camera.main, hit.point);
                jointTrans = AttachJoint(hit.rigidbody, hit.point);

                gameObject.layer = LayerMask.NameToLayer("Handling");
                dragging = true;

                PlayerController.instance.currentObject = this;

        }
    }

    public void HandleThrow(float _throwForce)
    {
        OnMouseUp();
        rb.AddForce(CameraController.instance.transform.forward * _throwForce, ForceMode.Impulse);
    }

    public virtual void HandleInput(Vector3 screenPosition)
    {
        if (jointTrans == null)
            return;
        //var worldPos = Camera.main.ScreenToWorldPoint(screenPosition);
        jointTrans.position = CameraPlane.ScreenToWorldPlanePoint(Camera.main, dragDepth, screenPosition);

    }

    public void HandleInputEnd()
    {
        if (dragging)
        {

            if (dropSound != null)
                objectAudioSource.PlayOneShot(dropSound);


            dragging = false;



            gameObject.layer = originalLayer; //LayerMask.NameToLayer("Interactive");


            PlayerController.instance.currentObject = null;

            Destroy(jointTrans.gameObject);
            joint = null;

        }
    }

    private Transform AttachJoint(Rigidbody rb, Vector3 attachmentPosition)
    {
        GameObject go = new GameObject("Attachment Point");
        go.hideFlags = HideFlags.HideInHierarchy;
        go.transform.position = attachmentPosition;

        Rigidbody newRb = go.AddComponent<Rigidbody>();
        newRb.isKinematic = true;

        joint = go.AddComponent<ConfigurableJoint>();
        joint.connectedBody = rb;
        joint.configuredInWorldSpace = true;
        joint.xDrive = NewJointDrive(force, damping);
        joint.yDrive = NewJointDrive(force, damping);
        joint.zDrive = NewJointDrive(force, damping);
        joint.slerpDrive = NewJointDrive(force, damping);
        joint.rotationDriveMode = RotationDriveMode.Slerp;

        return go.transform;
    }

    private JointDrive NewJointDrive(float force, float damping)
    {
        JointDrive drive = new JointDrive
        {
            //drive.mode = JointDriveMode.Position;
            positionSpring = force,
            positionDamper = damping,
            maximumForce = Mathf.Infinity
        };
        return drive;
    }
}