﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placeholder : MonoBehaviour
{

    public InteractionCheck interaction;

    public float flashSpeed = 0.01f;

    private MeshRenderer mRend;

    private bool alphaOn = true;

    // Use this for initialization
    void Start()
    {

        mRend = gameObject.GetComponent<MeshRenderer>();
        alphaOn = true;

        if (interaction == null)
        {
            transform.parent.gameObject.GetComponent<InteractionCheck>();
        }

    }

    // Update is called once per frame
    void Update()
    {

        Color color = mRend.material.color;

        if (alphaOn)
            color.a += flashSpeed;
        else
            color.a -= flashSpeed;

        mRend.material.color = color;

        if (color.a <= 0.1f)
            alphaOn = true;
        else if (color.a >= 0.9f)
            alphaOn = false;



    }
}
