﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public AudioQueue interactionQueue;

    public static AudioManager instance = null;

    public AudioSource backgroundRadioSource, interactionRadioSource;

    public float requiredCollisionForce = 2;

    public float queueWaitTime = 0;


    private void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        interactionQueue = GetComponent<AudioQueue>();

    }


    public void UpdateRadioPause()
    {

        if (backgroundRadioSource.isPlaying)
            backgroundRadioSource.Pause();

        StopAllCoroutines();

        //StopCoroutine(ResumeRadio(queueWaitTime));

        queueWaitTime = interactionQueue.GetQueueLength() - interactionRadioSource.time;

        StartCoroutine(ResumeRadio(queueWaitTime));



    }

    IEnumerator ResumeRadio( float waitTime)
    {

        yield return new WaitForSeconds(waitTime);

        backgroundRadioSource.UnPause();


    }
}
