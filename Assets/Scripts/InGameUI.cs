﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameUI : MonoBehaviour
{


    public GameObject inGamePanel, pausePanel, settingsPanel;

    public GameObject[] postitButtons;

    public Animator fadeAnimator;


    private void Start()
    {

        GameManager.instance.ingameUIMan = this;
        GameManager.instance.TogglePause();
        StopAllCoroutines();
        StartCoroutine(FinishLoading());
        AudioListener.volume = 0;

    }


    IEnumerator FinishLoading()
    {

        yield return new WaitForSeconds(2);
        GameManager.instance.isLoading = false;
        AudioListener.volume = 1;
        AudioManager.instance.backgroundRadioSource.Play();

    }


    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape) & !CameraController.instance.pausingGame & !CameraController.instance.resumingGame & !GameManager.instance.isLoading)
        {
            GameManager.instance.TogglePause();
        }

    }

    public void TogglePauseMenu()
    {

        foreach (GameObject postit in postitButtons)
        {
            postit.SetActive(!postit.activeSelf);
        }

    }


    public void ToggleSettingsMenu()
    {

        settingsPanel.SetActive(!settingsPanel.activeSelf);
        pausePanel.SetActive(!pausePanel.activeSelf);

    }


    public void ExitToMenu()
    {

        fadeAnimator.SetTrigger("Fade");

    }

}
