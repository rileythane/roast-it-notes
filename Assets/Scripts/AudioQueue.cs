/*
 * All Code Below Created By Lachee
 * website: www.lacheedomain.netii.net
 * email: lachee@lacheedomain.netii.net
 * 
 * Enjoy, no real rules except credit is appriciated if used on comertial project, but not nessary and do not claim as your own.
 * 
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
[AddComponentMenu("Audio/Audio Queue")]
public class AudioQueue : MonoBehaviour
{

    //public AudioSource radioSource;

    protected List<AudioClip> clipQueue = new List<AudioClip>();
    protected bool playQueue;

    public float Delay = 0F;


    public void AddToQueue(AudioClip clip)
    {
        clipQueue.Add(clip);
    }

    public void AddToQueue(AudioClip[] clips)
    {
        foreach (AudioClip clip in clips)
        {
            clipQueue.Add(clip);
        }
    }


    public void RemoveFromQueue(int index)
    {
        clipQueue.RemoveAt(index);
    }

    public void RemoveFromQueue(AudioClip clip)
    {
        clipQueue.Remove(clip);
    }

    public AudioClip CurrentClip()
    {
        return clipQueue[0];
    }

    public AudioClip NextClip()
    {

        return clipQueue[1];
    }

    public void PlayNextClip()
    {
        clipQueue.RemoveAt(0);
        if (clipQueue.Count > 0)
        {
            AudioManager.instance.interactionRadioSource.clip = clipQueue[0];
            AudioManager.instance.interactionRadioSource.Play();
            playQueue = true;
        }
        else
        {
            Stop();
        }

        op = false;
    }

    public void Stop()
    {
        AudioManager.instance.interactionRadioSource.Stop();
        CancelInvoke("PlayNextClip");
        playQueue = false;
    }

    public void PlayQueue()
    {
        if (clipQueue.Count <= 0)
        {
            Debug.LogWarning("No Clips Have Been Found In Queue");
            return;
        }
        if (clipQueue.Count > 0)
        {
            AudioManager.instance.interactionRadioSource.clip = clipQueue[0];
            AudioManager.instance.interactionRadioSource.Play();
            playQueue = true;
        }
    }

    public float GetQueueLength()
    {
        float queueTime = 0;

        foreach (AudioClip clip in clipQueue)
        {
            queueTime = queueTime + clip.length;
        }

        return queueTime;
    }

    public bool IsPlaying()
    {
        return playQueue;
    }

    public int QueueCount()
    {
        return clipQueue.Count;
    }

    bool op = false;
    void Update()
    {
        if (AudioManager.instance.interactionRadioSource != null)
        {
            if (!op && playQueue && !AudioManager.instance.interactionRadioSource.isPlaying)
            {
                op = true;
                if (Delay < 0)
                    Delay = 0;

                Invoke("PlayNextClip", Delay);

            }
        }
    }
}
